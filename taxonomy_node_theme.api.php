<?php

/**
 * @file
 * Hooks provided by the Taxonomy Node Theme module.
 */

/**
 * Adds a custom theme hook for the node represented by $variables.
 *
 * @param array $variables
 *   Array of node variables.
 */
function hook_taxonomy_node_theme_add_template(array &$variables) {
  $variables['theme_hook_suggestions'] = 'my_theme_hook';
}
