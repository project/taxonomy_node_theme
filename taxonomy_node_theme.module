<?php

/**
 * @file
 * Taxonomy node theme module main functions.
 */

/**
 * Implements hook_permission().
 */
function taxonomy_node_theme_permission() {
  $permissions = [];
  $permissions['administrate taxonomy node theme'] = [
    'title' => t('Administrate Taxonomy Node Theme Module'),
    'description' => t('Allows to use the Taxonomy Node Theme settings.'),
  ];

  return $permissions;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function taxonomy_node_theme_preprocess_node(array &$variables) {

  if (FALSE !== strpos((string) current_path(), 'taxonomy')) {

    $tid = (int) preg_replace('/\D/', '', (string) current_path());

    $nodes = taxonomy_select_nodes($tid);
    if (
      TRUE === $variables['teaser'] &&
      TRUE === in_array($variables['node']->nid, $nodes, TRUE)
    ) {
      $term = taxonomy_term_load($tid);
      $vocab = taxonomy_vocabulary_load($term->vid);

      $variables['theme_hook_suggestions'][] = 'node__vocabulary_' . $term->vid;
      $variables['theme_hook_suggestions'][] = 'node__vocabulary_' . $vocab->machine_name;
      $variables['theme_hook_suggestions'][] = 'node__taxonomy_term_' . $term->tid;
      $variables['theme_hook_suggestions'][] = 'node__taxonomy_term_' . taxonomy_node_theme_drupal_machine_name($term->name);
    }

    foreach(module_implements('taxonomy_node_theme_add_template') as $module) {
      $hook = sprintf('%s_taxonomy_node_theme_add_template', $module);
      $hook($variables);
    }
  }
}

/**
 * {@inheritdoc}
 *
 * @param string $name
 *   String to be converted into a machine name.
 *
 * @return string
 *   The converted string to be used as a Drupal machine name.
 */
function taxonomy_node_theme_drupal_machine_name($name) {

  $machine_name = strtolower($name);
  $machine_name = preg_replace('/[^a-z0-9_]+/', '_', $machine_name);
  return preg_replace('/_+/', '_', $machine_name);
}

/**
 * {@inheritdoc}
 *
 * @param \stdClass $node
 *   Drupal node object.
 * @param string $key
 *   Key to check, tid, vid.
 *
 * @return array
 *   Array of taxonomy terms matching node id.
 */
function taxonomy_node_theme_node_get_terms(\stdClass $node, $key = 'tid') {

  static $terms;

  if (FALSE === isset($terms[$node->vid][$key])) {
    $query = db_select('taxonomy_index', 'i');
    $term_alias = $query->join('taxonomy_term_data', 't', 'i.tid = t.tid');
    $query->join('taxonomy_vocabulary', 'v', 't.vid = v.vid');
    $query->fields($term_alias);
    $query->condition('i.nid', $node->nid);
    $result = $query->execute();
    $terms[$node->vid][$key] = [];
    foreach ($result as $term) {
      $terms[$node->vid][$key][$term->$key] = $term;
    }
  }

  return $terms[$node->vid][$key];
}
