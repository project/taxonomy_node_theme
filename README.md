Taxonomy Node Theme
===================

Adds theme hooks for separating out taxonomy terms.  As far as I could find, you
can have a template for theming vocabulary, but not taxonomy. The taxonomy-term.
tpl.php does not allow for theming the node teaser.  For this, you have to add
checks in the template for the tid.

This module makes it easier to separate this out by creating hooks as
node__taxonomy_{tid} and node__taxonomy_{machine_name}.  Because by default
taxonomy terms do not have a machine name, we create the machine name ourselves
by converting the term name to a machine name.  So 'My Cool Term' becomes
my_cool_term, and thus the template would be node--taxonomy-my-cool-term.tpl.
php or if the tid is 15 would also add node--taxonomy-15.tpl.php. Also if the
vocabulary of the term 'My Cool Term' was events, we make a template called node
--vocabulary-events.tpl.php.
